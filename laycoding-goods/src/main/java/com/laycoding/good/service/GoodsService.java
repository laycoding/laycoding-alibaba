package com.laycoding.good.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.laycoding.common.entity.Goods;
import com.laycoding.common.util.ResultJson;

import java.util.List;

public interface GoodsService extends IService<Goods> {
    ResultJson decrGoodNum(String goodId);
}
