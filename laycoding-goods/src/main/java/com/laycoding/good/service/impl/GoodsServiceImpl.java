package com.laycoding.good.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.laycoding.common.dao.GoodsMapper;
import com.laycoding.common.entity.Goods;
import com.laycoding.common.util.ResultJson;
import com.laycoding.good.service.GoodsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {
    @Override
    @Transactional
    public ResultJson decrGoodNum(String goodId) {

        QueryWrapper<Goods> queryWrapper =new QueryWrapper<>();
        queryWrapper.eq("good_id",goodId);
        Goods goods = this.getBaseMapper().selectOne(queryWrapper);
        goods.setGoodNum(goods.getGoodNum()-1);
        this.getBaseMapper().update(goods,queryWrapper);
        ResultJson<Goods> result = new ResultJson<>(200, "获取成功", goods);
        return result;
    }
}
