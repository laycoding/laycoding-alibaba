package com.laycoding.good.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.laycoding.common.entity.Goods;
import com.laycoding.common.util.ResultJson;
import com.laycoding.good.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/good")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @RequestMapping("/selectGoodById")
    public ResultJson selectGoodById(HttpServletRequest request, String goodId){
        System.out.println(request.getHeader("authorization").toString());
        QueryWrapper<Goods> queryWrapper =new QueryWrapper<>();
        queryWrapper.eq("good_id",goodId);
        Goods goods = goodsService.getBaseMapper().selectOne(queryWrapper);
        ResultJson<Goods> result = new ResultJson<>(200, "获取成功", goods);
        return result;
    }

    @RequestMapping("/decrGoodNum")
    public ResultJson decrGoodNum(String goodId){
        ResultJson resultJson = goodsService.decrGoodNum(goodId);
        return resultJson;
    }


}
