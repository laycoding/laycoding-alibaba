package com.laycoding.order.controller;


import com.laycoding.common.entity.Order;
import com.laycoding.common.util.ResultJson;
import com.laycoding.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping("/insertOrder")
    public ResultJson insertOrder(String orderId){
        Integer integer = orderService.insertOrder(orderId);
        return new ResultJson(200,"下单成功",integer);
    }
    @RequestMapping("/test")
    public ResultJson insertOrder1(){
        return new ResultJson(200,"下单成功",null);
    }
}
