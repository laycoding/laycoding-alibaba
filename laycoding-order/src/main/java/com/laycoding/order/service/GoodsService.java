package com.laycoding.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.laycoding.common.entity.Goods;
import com.laycoding.common.util.ResultJson;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "laycoding-good",qualifier = "goodService")
public interface GoodsService {
    @RequestMapping("/good/selectGoodById")
    ResultJson<Goods> selectGoodById(@RequestParam("goodId") String goodId);
    @RequestMapping("/good/decrGoodNum")
    ResultJson decrGoodNum(@RequestParam("goodId") String goodId);
}