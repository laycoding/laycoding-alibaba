package com.laycoding.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.laycoding.common.dao.OrderMapper;
import com.laycoding.common.entity.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;


public interface OrderService extends IService<Order> {
    Integer insertOrder(String orderId);
}
