package com.laycoding.order.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.laycoding.common.dao.OrderMapper;
import com.laycoding.common.entity.Goods;
import com.laycoding.common.entity.Order;
import com.laycoding.common.util.ResultJson;
import com.laycoding.order.service.GoodsService;
import com.laycoding.order.service.OrderService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.LinkedHashMap;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Autowired
    private GoodsService goodsService;
    @Override
    @GlobalTransactional(rollbackFor = Exception.class)
    public Integer insertOrder(String orderId) {
        ResultJson<Goods> resultJson = goodsService.selectGoodById(orderId);
      //  LinkedHashMap map = (LinkedHashMap) resultJson.getData();

       /* if (Integer.valueOf(map.get("goodNum").toString()) > 1) {
            Order order = new Order();
            order.setId(1);
            order.setOrderId("order_123456");
            order.setOrderDetail("2021003");
            order.setGoodId("good_123456");
            order.setUserId(10086);
            Integer integer = this.baseMapper.insert(order);
            ResultJson resultJson1 = goodsService.decrGoodNum(orderId);
          //  int i = 1 / 0;
        }*/
        return 1;
    }
}
