package com.laycoding.order.config;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobleException {

    @ExceptionHandler(value = Exception.class)
    public Map<String,Object> exceptionHandler(HttpServletRequest request, Exception e) {
           Map<String,Object> map =new HashMap<>();
           map.put("code",200);
           map.put("message","接口不存在~");
           map.put("data",null);

           return map;
    }
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public Map<String,Object> handle404(NoHandlerFoundException e) {
        Map<String,Object> map =new HashMap<>();
        map.put("code",200);
        map.put("message","接口不存在~");
        map.put("data",null);
        return map;
    }

}
