package com.laycoding.engine.controller;


import com.alibaba.fastjson.JSONObject;
import com.laycoding.common.entity.Goods;
import com.laycoding.common.entity.Order;
import com.laycoding.common.util.ResultJson;
import com.laycoding.engine.service.GoodsService;
import com.laycoding.engine.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.LinkedHashMap;

@RefreshScope
@RestController
@RequestMapping("/product")
public class TestController {

    @Value("${spring.datasource.url}")
    private String url;
    @Resource
    private OrderService orderService;
    @Resource
    private GoodsService goodsService;
    @Value("${server.port}")
    private Integer port;

    @RequestMapping("/test")
    public String getUrl() {
        return url + port;
    }

    @RequestMapping("/order")

    public ResultJson insertOrder(String goodId) {

        return new ResultJson(200, "下单成功", null);
    }

}
