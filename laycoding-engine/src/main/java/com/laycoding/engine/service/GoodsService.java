package com.laycoding.engine.service;


import com.laycoding.common.util.ResultJson;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "laycoding-good")
public interface GoodsService {
    @RequestMapping("/good/selectGoodById")
    ResultJson selectGoodById(String goodId);

}
