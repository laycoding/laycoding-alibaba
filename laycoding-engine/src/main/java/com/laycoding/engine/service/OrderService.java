package com.laycoding.engine.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.laycoding.common.entity.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("laycoding-order")
public interface OrderService {

    @RequestMapping("/insertOrder")
    Integer insertOrder(Order order);
}
