package com.laycoding.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("ly_good")
public class Goods implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String goodId;

    private Integer goodNum;

    private String goodDetail;

    private Double price;

    private Date create_time;
}
