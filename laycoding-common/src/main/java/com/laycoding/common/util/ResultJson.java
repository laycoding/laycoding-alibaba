package com.laycoding.common.util;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResultJson<T> implements Serializable {

    private Integer code;

    private String message;

    private T data;

    public ResultJson(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
