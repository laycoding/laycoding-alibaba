package com.laycoding.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.laycoding.common.entity.Goods;


public interface GoodsMapper extends BaseMapper<Goods> {
}
