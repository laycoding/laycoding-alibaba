package com.laycoding.common.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.laycoding.common.entity.Order;
import org.springframework.stereotype.Repository;



public interface OrderMapper extends BaseMapper<Order> {
}
