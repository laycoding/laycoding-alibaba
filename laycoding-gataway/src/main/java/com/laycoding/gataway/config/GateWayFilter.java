package com.laycoding.gataway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;

import org.springframework.core.Ordered;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.lang.annotation.Annotation;
import java.util.List;

@Component
public class GateWayFilter implements GlobalFilter , Ordered {


    @Autowired
    private RedisTemplate redisTemplate;

    private RestTemplate  restTemplate;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

       // restTemplate.exchange()

        Object guidada = redisTemplate.opsForValue().get("guidada");

        ServerHttpRequest request = exchange.getRequest();

        String value = request.getPath().value();
        MultiValueMap<String, String> queryParams = request.getQueryParams();
        HttpHeaders headers = request.getHeaders();

        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
